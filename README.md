# SSO #

### How do I get set up? ###

    npm install git+https://laukikpatel@bitbucket.org/laukikpatel/sso-pwa.git --save-dev

### Environment variable ###

    SSO_SERVER='https://accounts.globalgarner.com'

**Example:**

    import SSO from 'sso';
    
    class App extends Component {
    
        requireAuth(nextState, replace, next)
        {
            SSO.isLogin(function(isLogin) {
                if(isLogin === true) {
                    next();
                }
            });
        }
    
        render(){
            return(
                <MuiThemeProvider>
                    <Router history={hashHistory}>
                        <Route path='/' component={Sources} onEnter={this.requireAuth} />
                        <Route path='/articles/:source_id' component={Articles} onEnter={this.requireAuth} />
                    </Router>
                </MuiThemeProvider>
            );
    
        }
    
    }

### Helper ###

    Get User Data
    SSO.profile(function(userData) {
        console.log(userData);
    });
    
    Logout
    SSO.logout();
    
    Redirect to Login Page
    SSO.login();
    
    Call api using GET Method
    SSO.GET('https://xxx.globlagarner.com/api/xxxx', function(data) {
        console.log(data);
    })
    
    Call api using POST Method
    SSO.POST('https://xxx.globlagarner.com/api/xxxx', {key:'val', 'key1':'val1'}, function(data) {
        console.log(data);
    })
    
    Call api using PUT Method
    SSO.PUT('https://xxx.globlagarner.com/api/xxxx', {key:'val', 'key1':'val1'}, function(data) {
        console.log(data);
    })
    
    Call api using DELETE Method
    SSO.DELETE('https://xxx.globlagarner.com/api/xxxx', {key:'val', 'key1':'val1'}, function(data) {
        console.log(data);
    })

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact