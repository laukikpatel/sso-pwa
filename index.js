var SSO = (function () {

    const SSO_SERVER = process.env.SSO_SERVER || 'http://accounts.gg.dev';
    var user;
    var is_logged_in;

    function init(callback) {
        fetch(SSO_SERVER+'/api/user/profile', {
            credentials: 'include',
            headers: {
                Accept: 'application/json'
            }
        })
            .then(function(response) {
                if(response.status === 401) {
                    throw new Error('Unauthorized', 401);
                }
                return response.json();
            })
            .then(function(data) {
                if(!data.user_id) {
                    throw new Error('Unauthorized', 401);
                }
                user = data;
                is_logged_in = true;
                callback();
            })
            .catch(function (e) {
                window.location.href = SSO_SERVER+'/login?continue='+encodeURIComponent(window.location.href);
            });
    }

    function onResponse(res) {
        if(res.status === 401) {
            throw new Error('Unauthorized', 401);
        }
        return res.json();
    }

    function onError(e, callback) {
        if(e.code == 401 || e.message == 'Unauthorized') {
            window.location.href = SSO_SERVER+'/login?continue='+encodeURIComponent(window.location.href);
        } else {
            callback({error: e.message});
        }
    }

    return {
        isLogin: function (callback) {
            if( typeof is_logged_in === 'undefined') {
                init(function() {
                    callback(is_logged_in);
                });
            } else {
                callback(is_logged_in);
            }
        },

        profile: function(callback) {
            if( typeof user === 'undefined') {
                init(function() {
                    callback(user);
                });
            } else {
                callback(user);
            }
        },

        login: function() {
            window.location.href = SSO_SERVER+'/login?continue='+encodeURIComponent(window.location.href);
        },

        logout: function() {
            window.location.href = SSO_SERVER+'/logout?continue='+encodeURIComponent(window.location.href);
        },

        GET: function(url, callback) {
            fetch(url, {
                credentials: 'include',
                headers: {
                    Accept: 'application/json'
                }
            })
                .then(function(res) {
                    return onResponse(res);
                })
                .then(function(data) {
                    callback(data);
                })
                .catch(function(e) {
                    onError(e, callback);
                });
        },

        POST: function(url, data, callback) {
            fetch(url, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    Accept: 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(function(res) {
                    return onResponse(res);
                })
                .then(function(data) {
                    callback(data);
                })
                .catch(function(e) {
                    onError(e, callback);
                });
        },

        PUT: function(url, data, callback) {
            fetch(url, {
                method: 'PUT',
                credentials: 'include',
                headers: {
                    Accept: 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(function(res) {
                    return onResponse(res);
                })
                .then(function(data) {
                    callback(data);
                })
                .catch(function(e) {
                    onError(e, callback);
                });
        },

        DELETE: function(url, data, callback) {
            fetch(url, {
                method: 'DELETE',
                credentials: 'include',
                headers: {
                    Accept: 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(function(res) {
                    return onResponse(res);
                })
                .then(function(data) {
                    callback(data);
                })
                .catch(function(e) {
                    onError(e, callback);
                });
        }
    };
})();

export default SSO;